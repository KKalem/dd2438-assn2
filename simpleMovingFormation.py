# -*- coding: utf-8 -*-
"""
Created on Thu Feb 25 15:22:55 2016

@author: kkalem
"""
import matlabHandler as mh
import polySearch as ps
import models as md
import matplotlib.pyplot as plt
import views as vw
import formation as form
import numpy as np

s,g,x,y,edges,button,customer = mh.generateOBJstr('assn2map1.mat') #assn2map1 or map2
polygons = ps.polysToVerts(x,y,button)
mh.draw2Dpolygons(x,y,edges,False)

view = vw.TracingView()
model = md.dyn_point(view,100.,1.)


models = []
formation = form.formation([[-1,-1],[0,0],[1,-1]],d=5,velocity=[0,5])

for i in range(3):
    models.append(md.dyn_point(vw.TracingView(),100.,1.))
    models[i].setPosition([10.*i,10.*i,0.])
    models[i].view.reinit()


for i in range(1500):

    formation.tick()
    formation.draw()
    modelposs = [models[k].position for k in range(len(models))]
    indices = formation.compute(modelposs)
    for j in range(len(models)):
        models[j].setGoal(formation.real[indices[j][1]], 0.1, True)
    for j in range(len(models)):
        models[j].tickAndSync()
#        models[j].tick()
#        models[j].view.setPosition(models[j].position)
#        models[j].view.syncSignal()

    formation.moveTo([models[1].position[0],10+models[1].position[1]]) # keep the formation relative to a model
    if i%30 == 0:
        plt.pause(0.001)
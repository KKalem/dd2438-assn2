# -*- coding: utf-8 -*-
"""
Created on Sat Mar 05 16:00:46 2016

@author: Miquel
"""
import matlabHandler as mh
import views as vw
import formation as form
import dynamic_point as dp
import geometry as geom
import numpy as np
import vrep

import matplotlib.pyplot as plt

s,g,x,y,edges,button,customer = mh.generateOBJstr('assn2map1.mat') #assn2map1 or map2

R=150
doInVrep=True
scale=0.01
baseName='port_sphere'

starting = s.tolist() + g.tolist()
if doInVrep:
    cid = vw.startVrep()
    err, handle = vrep.simxGetObjectHandle(cid, baseName, vrep.simx_opmode_oneshot_wait)
    objHandle=[handle]
    for st in starting:
        err, newFloorHandle = vrep.simxCopyPasteObjects(cid, [objHandle[0]] , vrep.simx_opmode_oneshot_wait)
        vrep.simxSetObjectPosition(cid, newFloorHandle[0], -1, (np.array(st)*scale).tolist(), vrep.simx_opmode_oneshot)
        objHandle.extend(newFloorHandle)



agents = []
i=0
for pos in starting:
    if doInVrep:
        view = vw.vrep_view(cid,objHandle[i+1],scale)
    else:
        view = vw.TracingView(init_pos = geom.in3d(pos), cont = True)    
    agent = dp.DynamicPoint(view, i, X = pos, size = 0.)
    agent.view.reinit()
    agents.append(agent)
    i+=1

#BRING TOGETHER
while any(len(a.veh_in_range)<len(agents) for a in agents):
    for agent in agents:
        #Sense surroundings
        inrange=[a for a in agents if np.abs(np.linalg.norm(a.X-agent.X))<R]
        agent.addVehiclesRange(inrange)
        #Compute center of mass
        fx = 0.
        fy = 0.
        for v in agent.veh_in_range.values():
            fx += v.X[0]
            fy += v.X[1]
        fx = fx / len(agent.veh_in_range)
        fy = fy / len(agent.veh_in_range)
        #Set center of mass as target
        agent.setTarget([fx,fy])
    for agent in agents:
        #Tick
        agent.tick()
    agents[0].view.syncSignal()
    


if doInVrep:
    view = vw.vrep_view(cid,'',scale)
else:
    for agent in agents:
        r = agent.size
        c1 = plt.Circle(agent.X,r, fill = False)
        fig = plt.gcf()
        fig.gca().add_artist(c1)
    view = vw.TracingView()

f = form.formation([[0,0],[1,0],[2,0],[3,0],[0,1],[1,1],[2,1],[3,1],[1,2],[2,2]],d=50, view = view, velocity = [0,10], pos = [fx-100,fy-70])

for time in range(1600):
#    plt.clf()
    modelposs = [agents[k].X for k in range(len(agents))]
    indices = f.compute(modelposs)
    for j in range(len(agents)):
        agents[j].setTarget(f.real[indices[j][1]])
        agents[j].tick()
#        plt.scatter(agents[j].X[0],agents[j].X[1])
    f.tick()
    if time%30==0 and not doInVrep:
        plt.pause(0.001)
        f.draw()

#for agent in agents:
#    vw.drawPath(agent.view.pos_trace)
#    plt.scatter(agent.X[0], agent.X[1])
#f.draw()